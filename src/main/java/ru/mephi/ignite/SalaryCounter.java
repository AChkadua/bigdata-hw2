package ru.mephi.ignite;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.binary.BinaryObject;
import org.apache.ignite.cache.query.QueryCursor;
import org.apache.ignite.cache.query.ScanQuery;
import org.apache.ignite.lang.IgniteBiPredicate;
import org.apache.ignite.lang.IgniteRunnable;
import org.apache.ignite.resources.IgniteInstanceResource;
import ru.mephi.ignite.dto.Passport;
import ru.mephi.ignite.dto.Paycheck;
import ru.mephi.ignite.dto.Trip;

import javax.cache.Cache;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Salary and trips counter.
 * Counts average salaries and trips for different age gaps.
 */
public class SalaryCounter implements IgniteRunnable {

    public static final String YOUTH = "Youth";
    public static final String MIDDLE_AGED = "Middle aged";
    public static final String SENIOR = "Senior";

    @IgniteInstanceResource
    private Ignite ignite;

    private Map<String, Double> averageSalaries;
    private Map<String, Double> averageTrips;

    /**
     * Creates new salary and trips counter.
     * @param averageSalariesContainer container to output average salaries to
     * @param averageTripsContainer container to output average trips to
     */
    public SalaryCounter(Map<String, Double> averageSalariesContainer, Map<String, Double> averageTripsContainer) {
        averageSalaries = averageSalariesContainer;
        averageTrips = averageTripsContainer;
    }

    /**
     * Start job.
     */
    @Override
    public void run() {
        IgniteCache<BinaryObject, BinaryObject> passportsCache = ignite.cache("passports").withKeepBinary();
        IgniteCache<BinaryObject, BinaryObject> paychecksCache = ignite.cache("paychecks").withKeepBinary();
        IgniteCache<BinaryObject, BinaryObject> tripsCache = ignite.cache("trips").withKeepBinary();
        ScanQuery<BinaryObject, BinaryObject> query = new ScanQuery<>();
        List<Passport> passports = new ArrayList<>();
        try (QueryCursor<IgniteCache.Entry<BinaryObject, BinaryObject>> cursor = passportsCache.query(query)) {
            for (Cache.Entry entry : cursor.getAll()) {
                String key = (String) entry.getKey();
                BinaryObject value = (BinaryObject) entry.getValue();
                passports.add(new Passport(key, value.<Integer>field("AGE")));
            }
        }

        Map<Passport, List<Paycheck>> paychecksGrouped = new HashMap<>();
        Map<Passport, List<Trip>> tripsGrouped = new HashMap<>();

        for (Passport passport : passports) {
            ScanQuery<BinaryObject, BinaryObject> salaryQuery =
                    new ScanQuery<>(new PassportFilter(passport.getNumber()));
            try (QueryCursor<IgniteCache.Entry<BinaryObject, BinaryObject>> cursor =
                         paychecksCache.query(salaryQuery)) {
                List<Paycheck> paychecks = cursor.getAll().stream()
                        .map(e -> new Paycheck(passport.getNumber(),
                                e.getKey().<Integer>field("MONTH"),
                                e.getValue().<Integer>field("SALARY")))
                        .collect(Collectors.toList());
                paychecksGrouped.put(passport, paychecks);
            }

            ScanQuery<BinaryObject, BinaryObject> tripsQuery =
                    new ScanQuery<>(new PassportFilter(passport.getNumber()));
            try (QueryCursor<IgniteCache.Entry<BinaryObject, BinaryObject>> cursor =
                         tripsCache.query(tripsQuery)) {
                List<Trip> trips = cursor.getAll().stream()
                        .map(e -> new Trip(passport.getNumber(),
                                e.getKey().<Integer>field("MONTH"),
                                e.getValue().<Integer>field("TRIPS")))
                        .collect(Collectors.toList());
                tripsGrouped.put(passport, trips);
            }
        }

        Map<String, List<Passport>> passportsGrouped = new HashMap<>();
        passportsGrouped.put(YOUTH, new ArrayList<>());
        passportsGrouped.put(MIDDLE_AGED, new ArrayList<>());
        passportsGrouped.put(SENIOR, new ArrayList<>());
        for (Passport passport : passports) {
            if (AgeGap.YOUTH.getPredicate().test(passport)) {
                passportsGrouped.get(YOUTH).add(passport);
            } else if (AgeGap.MIDDLE_AGED.getPredicate().test(passport)) {
                passportsGrouped.get(MIDDLE_AGED).add(passport);
            } else if (AgeGap.SENIOR.getPredicate().test(passport)) {
                passportsGrouped.get(SENIOR).add(passport);
            }
        }

        for (Map.Entry<String, List<Passport>> entry : passportsGrouped.entrySet()) {
            int divider = entry.getValue().size() * 12;
            double totalSalary = 0;
            double totalTrips = 0;
            for (Passport passport : entry.getValue()) {
                totalSalary += paychecksGrouped.get(passport).stream().mapToInt(Paycheck::getSalary).sum();
                totalTrips += tripsGrouped.get(passport).stream().mapToInt(Trip::getTripsNumber).sum();
            }
            averageSalaries.put(entry.getKey(), totalSalary / divider);
            averageTrips.put(entry.getKey(), totalTrips / divider);
        }
    }

    /**
     * Filter "by passport number" for queries.
     * Is a separate class to prevent caching on Ignite side.
     */
    @AllArgsConstructor
    class PassportFilter implements IgniteBiPredicate<BinaryObject, BinaryObject> {

        private String number;

        @Override
        public boolean apply(BinaryObject key, BinaryObject value) {
            return key.<String>field("PASSPORT").equals(number);
        }
    }

    /**
     * Age gaps.
     */
    @AllArgsConstructor
    @Getter
    private enum AgeGap {
        YOUTH(p -> p.getAge() <= 30),
        MIDDLE_AGED(p -> p.getAge() <= 45 && p.getAge() > 30),
        SENIOR(p -> p.getAge() > 45);

        Predicate<Passport> predicate;
    }
}
