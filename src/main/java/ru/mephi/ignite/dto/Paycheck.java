package ru.mephi.ignite.dto;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

/**
 * Monthly paycheck info DTO.
 */
@Getter
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class Paycheck {
    /**
     * Passports number
     */
    private String passport;

    /**
     * Month number
     */
    private Integer month;

    /**
     * Month salary
     */
    private Integer salary;
}

