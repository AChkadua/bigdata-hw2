package ru.mephi.ignite.dto;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;


/**
 * Trips info DTO.
 */
@Getter
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class Trip {
    /**
     * Passport number
     */
    private String passport;

    /**
     * Month number
     */
    private Integer month;

    /**
     * Trips amount
     */
    private Integer tripsNumber;
}
