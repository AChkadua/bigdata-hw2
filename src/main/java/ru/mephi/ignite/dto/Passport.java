package ru.mephi.ignite.dto;

import lombok.*;

@Getter
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString
/**
 * Passport DTO
 */
public class Passport {
    /**
     * Passport number
     */
    @EqualsAndHashCode.Include
    private String number;

    /**
     * Age of person this passport belongs to
     */
    private Integer age;
}
