package ru.mephi.ignite;

import org.apache.ignite.Ignite;
import org.apache.ignite.Ignition;

import java.io.PrintWriter;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * Salary and trips counter application.
 * Runs the job to compute the average salaries and amount of trips for each age gap and outputs the result to stdout.
 */
public class SalaryCounterApp {
    private static final MessageFormat FORMAT = new MessageFormat("{0}, {1,number,#.##}, {2,number,#.##}");

    /**
     * Starts the compute job and outputs the results.
     * @param args command line arguments
     */
    public static void main(String[] args) {
        Map<String, Double> averageSalaries = new HashMap<>();
        Map<String, Double> averageTrips = new HashMap<>();
        try (Ignite ignite = Ignition.start("src/main/resources/salary-counter.xml")) {
            ignite.compute().run(new SalaryCounter(averageSalaries, averageTrips));
        }

        for (Map.Entry<String, Double> entry : averageSalaries.entrySet()) {
            String ageGap = entry.getKey();
            Double averageSalary = entry.getValue();
            System.out.println(FORMAT.format(new Object[]{ageGap, averageSalary, averageTrips.get(ageGap)}));
        }

        System.exit(0);
    }
}
