package ru.mephi.ignite;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.Ignition;
import org.apache.ignite.binary.BinaryObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import ru.mephi.ignite.dto.Passport;

import java.util.*;

import static org.junit.Assert.assertEquals;

/**
 * Tests for salary and trips counter.
 */
public class SalaryCounterTest {
    private Ignite ignite;

    private Map<String, Double> averageSalaries = new HashMap<>();
    private Map<String, Double> averageTrips = new HashMap<>();

    @Before
    public void setUp() {

        Random r = new Random();

        List<Passport> passports = new ArrayList<>();
        passports.add(new Passport("1234567890", 25));
        passports.add(new Passport("0987654321", 35));
        passports.add(new Passport("5432167890", 50));

        List<String> ageGaps = new ArrayList<>();
        ageGaps.add("Youth");
        ageGaps.add("Middle aged");
        ageGaps.add("Senior");

        this.ignite = Ignition.start("src/main/resources/salary-counter.xml");
        IgniteCache<String, BinaryObject> passportsCache = ignite.getOrCreateCache("passports").withKeepBinary();
        for (int i = 0; i < 3; i++) {

            BinaryObject value = ignite.binary()
                    .builder("Int")
                    .setField("AGE", passports.get(i).getAge())
                    .build();
            passportsCache.put(passports.get(i).getNumber(), value);
        }

        IgniteCache<BinaryObject, BinaryObject> paychecksCache = ignite.getOrCreateCache("paychecks").withKeepBinary();
        IgniteCache<BinaryObject, BinaryObject> tripsCache = ignite.getOrCreateCache("trips").withKeepBinary();
        for (int i = 0; i < 3; i++) {
            double totalSalary = 0;
            double totalTrips = 0;
            for (int j = 0; j < 12; j++) {
                int salary = r.nextInt(150000);
                int trips = r.nextInt(3);
                BinaryObject key = ignite.binary()
                        .builder("Paycheck")
                        .setField("PASSPORT", passports.get(i).getNumber())
                        .setField("MONTH", j)
                        .build();
                BinaryObject salaryValue = ignite.binary()
                        .builder("Int")
                        .setField("SALARY", salary)
                        .build();
                BinaryObject tripsValue = ignite.binary()
                        .builder("Int")
                        .setField("TRIPS", trips)
                        .build();

                paychecksCache.put(key, salaryValue);
                tripsCache.put(key, tripsValue);

                totalSalary += salary;
                totalTrips += trips;
            }

            averageSalaries.put(ageGaps.get(i), totalSalary / 12);
            averageTrips.put(ageGaps.get(i), totalTrips / 12);
        }
    }

    @Test
    public void testSalaryCounter() {
        Map<String, Double> actualAverageSalaries = new HashMap<>();
        Map<String, Double> actualAverageTrips = new HashMap<>();
        ignite.compute().run(new SalaryCounter(actualAverageSalaries, actualAverageTrips));

        for (Map.Entry<String, Double> entry : averageSalaries.entrySet()) {
            assertEquals(entry.getValue(), actualAverageSalaries.get(entry.getKey()));
            assertEquals(averageTrips.get(entry.getKey()), actualAverageTrips.get(entry.getKey()));
        }
    }

    @After
    public void tearDown() {
        this.ignite.close();
    }
}
