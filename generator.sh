#/bin/bash

if [[ $1 == '' ]]
then
	echo 'Usage: ./generator.sh <output_file>'
	exit 0
fi

file=$1
if [ -f $file ]
then
	read -p "File '"$1"' exists, overwrite? (y/N): " -r
	while [[ ! $REPLY =~ ^[yYnN]?$ ]]
	do
		read -p "Use 'n' or 'y' (case-insensitive): " -r
	done

	if [[ ! $REPLY =~ ^[Yy]$ ]]
	then
		echo 'Aborting'
		exit 0
	fi
fi

echo 'CREATE TABLE IF NOT EXISTS Passport(number VARCHAR(10) PRIMARY KEY, age INT NOT NULL) WITH "CACHE_NAME=passports";' > $1
echo 'CREATE TABLE IF NOT EXISTS Paycheck(passport VARCHAR(10) NOT NULL, month INT NOT NULL, salary INT NOT NULL, PRIMARY KEY (passport, month)) WITH "CACHE_NAME=paychecks";' >> $1
echo 'CREATE TABLE IF NOT EXISTS Trip(passport VARCHAR(10) NOT NULL, month INT NOT NULL, trips INT NOT NULL, PRIMARY KEY (passport, month)) WITH "CACHE_NAME=trips";' >> $1

num_passports=$(( ( RANDOM % 20 ) +10 ))
passports=[]
for (( i=0; i<=$num_passports; i++ ))
do
	passports[$i]=$(shuf -i 1000000000-9999999999 -n 1)
	echo 'INSERT INTO Passport(number, age) VALUES ('${passports[i]}', '$(( (RANDOM % 50) + 20 ))');' >> $1
done

for (( i=0; i<=$num_passports; i++ ))
do
	for (( j=1; j<=12; j++ ))
	do
		echo 'INSERT INTO Paycheck(passport, month, salary) VALUES ('${passports[i]}','$j','$(( $(shuf -i 3-20 -n 1) * 10000 ))');' >> $1
		echo 'INSERT INTO Trip(passport, month, trips) VALUES ('${passports[i]}','$j','$(( $(shuf -i 0-3 -n 1) ))');' >> $1
	done
done

exit 0
