# Average salaries and trips computer

Computes average salaries and amount of abroad trips for different age gaps (30-, 30-45, 45+).
By default outputs the results to stdout.

# Requirements

* Linux
* Java 8
* Apache Ignite 2.6.0

# How to start

1. Start Apache Ignite (the config is provided in `src/main/resources/salary-counter.xml`)
2. Activate cluster - run `${IGNITE_HOME}/bin/control.sh --activate`
3. Add data using sqlline - `${IGNITE_HOME}/bin/sqlline.sh -u jdbc:ignite:thin://<host>/`. Mock data generator 
is provided (`generator.sh`). It also creates the necessary tables.
4. Run `mvn compile exec:java`. On this step the app can fail with `NullPointerException(SalaryCounter:18)`. It is 
a line with import, so... it's a kind of magic, I guess. Just run it again until it works. It works, I promise!
